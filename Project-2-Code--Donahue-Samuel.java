/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptdragging;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author elisha
 * todo:  crib draging function,  
 */
public class Cryptdragging {
    //cypher texts
        static final String CT0 = "9d6e7a7d155295eef8512c087da56084f743aaa9985ee3848a768c3484d2e2ea6b3f4e5483f612d55987ff4782f360bd4809bab835fa1e65f8459c6814472508cc7f72241ee70c34fb9e7c8c4c246132845d5d73d070de99a73efe1e9ee627ea8f4aaae147087cce6c8cd08813f81caf9d7120486b16";
        static final String CT1 = "8968617d0f1b97e5ee51280f3aec72caf106bdb48d44a68291339e35db86b6f56f2140548bf253cd1593e8498baa34ba4f02fda070e25075b04387681b473610cc7f75614ae21a3efb8672d85d3e2031cc5b5a23c17ad9cda334ef10";
        static final String CT2 = "8f6f3779154f99e8e014375c34a267c1e745bbad895fe391c526873383cfa1e86632575481e803c41c93f94d97a738f54d02a9ec66bb1360b44ed3210206254fcc627b240bfb1d38b899788a093d2f2a9b4b1327c17edf99b224fe1e9ee66effc649a3b5430c7c9a2a91d7c51bab14e9d57d39566a5634c4340c858c5a93fff89e0394f3f49369ce9c6ee6f26b29a82e29fb9ca0a8657c48ad2c60eaf70a6b44918a40630392171ca29b87cd9e9e037c6ad2bc4be08c61ba6223a8bac0a024741b71b270a0579ab1d0308ce9b54b391a6a49ebacfc5eb5b57dd56caf0b1694544cfa6e";
        static final String CT3 = "9a697e6b415897fef902205c34a267d6fa42abbe985fe393972f963598c1b0fc7a3b4c17c2f101c51488f94199b667f54009b9eb7df40721ac4a963156473406cc62707406ea043cb586789c093f2f658d48433fc07ccacdaf23f54dd9a853e4df41ace6130e61986f8cd48156b11daad1692d433518349d2d0ec09a4ddafdf59e0882a4ffdd7cdd896bf7bb663ea83f37f59ca0e17a7a4aa96c25fee50d2b0d93de5f6719d91700e19e97d186d7077c2985f856ef8035ab7c66bdbadfee2a664e77bf66ac0788bac1748eacb45936196004a2b0e955b6a8798166e";
        static final String CT4 = "8a68717e085ed8eae515651438a07fc9f448feb49358b19f812385249386b6f56f73461b8ce216dc0dc1e24ecfa361b74d0ebeeb7efe0921bb508a38024921118d7b757d44af1d31bed270995d3e24288d4c5a30c8738b9bb23ef25d9caa27e4c908abfc550b678b2796d4891ab512a79d772c5f2f5d3f872802cb895a93f7abd51993e5ee9376dbd072f0f27b35e43f2ffb85b7e4773242bb337caee40067079f934477149c520bfa9c81cf97d01c6125c6f352f88833af7466babc98e3247f4b70ae7cee40c9b0cc2780bba25e325e694bacbfef59a5b27d8631";
        static final String CT5 = "9a6972380c5a91e5f80537193ca133c7e75faea9924bb191953e8f22d7c5adf067264b1d96f853c41892ad4480bd73f54902b1af35ef1860ac02972d05013543d93d306603fb4932be8b3d8f4825613183571320c170d9cde638f41e80e173e3dc5caefb574d6fce688cc49113f515a6cf7f2c066e4c3385230885884ddcf3f898029fe1e8dd3fcc9f76f3a77d35fa2d75b281b7e56b7f45bf3225e3ff003501d78d146e018e484ee18383d187ca0d6025d5f348ed9b61ae7f33acbfddf36b774d60a86ca016d1f4c83b8dbdaf59795e624dbdbbf310a5b271813fa31400835f4eec250ddd43de10a25e790c9f28e7ebd0c74fa11181fcf79cf68d5bd75b662bbacf38d31b39cbcdc71d213d611812188ebe8855dcf857337ac75e2b36cceb0c4a729e1b7c72e78e0962c112351e62aa0f41456ebd34667929cdb06c2ffd627d7b11da0b6dd57900d0cafd9651a32e8d4247a7ab3cce24d08150ceee321b38dae50481acf00896ce1f8c7b7499dcf79008c9b5aee24b8de4ff1737116c";
        static final String CT6 = "876f377f04559df9ea1d695c2db971c8fc45feb69855e393972f963598c1b0fc7a3b5c5491f800d81c8cfe089aa071f54906afaf38ef1f2cab4d9f3e130636118369716107fc4938a8d269904c7623249f514073c6798bcdae29bb5f9bef68f9c65ca7f81d4d5a866fdedc8a05ac53b9cf792d49625129852e17858f53d4f1aa9c1993e9bac770cb9162a3b46622a82e2ef09fbbeb2e7942a36066fce91f330b978c557208805207f1cc9cd29392487064d6f95ba8862fea642eabf3c8f2227f5e25bc74e35386a6d6748cafe75c320c7c04a7bfef57b4fa799b6baf1d06834901";
        static final String CT7 = "9c647079135f94eef80265133bec67ccf006b3bc8944a69d84228f2296cae2e962364a069ba111c91188e34ccfb27af5400bbaa467f20469b50ed33c1e436601897869240be30e36a99b6990442561249e5d1327c170d8dce638f35f83a866f9ca08b8f05f0123856491c68b56b91dad9d6b2c4a6315238b2316c88b51c7fbbcd50f9ee7fbc66ccad06febb77070e92c3eb292befb613250bf2c69a3e40a3410959a14630e9d5219e780828c81ca1d766cc0f811";
        static final String CT8 = "9d64746a044fd8e0ee08651f2fb563d0fa41acbc8d44bad08833922998c2b1bd6f3e55188df853cd5992e44688bf71f54a02a4eb73f40221ba4d8720564328009e726d7003e00779ba9c79d84d3322379548473ac67185";
        static final String CT9 = "8f2175740e5893abe818351438be33cde606adb2d04fa29c8933826195c3a1fc7f20405496e9168c0a82e54d82b634b04f04afb265ef0321b74c9668144a2900872b72624aeb082dbad27c8c0937613185555673dc6cc2d7a16cef5692a874eac24deffe56142e8164ded48415b053abd1732a4d21";
        static final String TARGET = "866e786a0042d4abf21e305c35ad65c1b542bbbe8f55b3848032c6359fc3e2e96b21421196a107c90195a30896bc61f54906abae35fd196fb1519b2d1206320b892b7c7719e60e37b697738c0776292a9c5d132ac66a8bd1a728bb5882e629";
    
    public static void main(String[] args) {

        //get the cypher texts to use 
        int cypherSelections[]; //inter array to hold the selected cypher texts 
        cypherSelections = getCypherTextSelection();
        String selectedCypherText1 = getSelectedCypherText(cypherSelections[0]);
        String selectedCypherText2 = getSelectedCypherText(cypherSelections[1]);
        
        //get the trimmed text 
        String trimmedSelectedCypherText1 = getTrimmedString(selectedCypherText1, selectedCypherText2);
        String trimmedSelectedCypherText2 = getTrimmedString(selectedCypherText2, selectedCypherText1);
        
        //xor the trimmed text
        //converts hex string to integer arrayList then XORs the interger values and converts it back to a hex string
        String xORedCypherTexts = getHexStringFromIntArray(getXorValues(getIntValuesFromHexString(trimmedSelectedCypherText1), getIntValuesFromHexString(trimmedSelectedCypherText2)));
        
        System.out.println(trimmedSelectedCypherText1);
        System.out.println("XOR");
        System.out.println(trimmedSelectedCypherText2);
        System.out.println("is");
        System.out.println(xORedCypherTexts);
        System.out.print("\n");
        System.out.println("========================================================================================================================");
        System.out.print("\n");
        
        boolean exit = false;
        while (!exit) {
            String CribText = getCribText();
           
            if("exit123".equals(CribText)){
                exit = true;
            }
            
            //get Hex string for the crypt text
            String HexCribText = getHexFromAscii(CribText);
            
            StartDragging(xORedCypherTexts, CribText);
            System.out.println("========================================================================================================================");
        }
        System.exit(0);
        //filter results
        
                

        

        
    }
    
    /**
     * ask user what cypher texts to use and return integer array with selection
     */
    private static int[] getCypherTextSelection (){
        int cypherSelections[] = new int[2]; //inter array to hold the selected cypher texts 
        
        Scanner cypherTextsInput = new Scanner(System.in); 
        System.out.print("Enter the firt cypher text to use (enter number 0-10, 10 is the target cypher text): ");
        cypherSelections[0] = cypherTextsInput.nextInt();
        
        System.out.print("Enter the second cypher text to use (enter number 0-10, 10 is the target cypher text): ");
        cypherSelections[1] = cypherTextsInput.nextInt();
        //cypherTextsInput.close();
        return cypherSelections;
    }
    
    /**
     * get the selected cypher text. returns a hex string
     */
    private static String getSelectedCypherText(int selection){
        switch(selection){
            case 0:
                return CT0;
            case 1: 
                return CT1;
            case 2: 
                return CT2;
            case 3: 
                return CT3;
            case 4: 
                return CT4;
            case 5: 
                return CT5;   
            case 6: 
                return CT6;    
            case 7: 
                return CT7;    
            case 8: 
                return CT8;    
            case 9: 
                return CT9;    
            case 10: 
                return TARGET;    
            default:
                System.err.println("error in getSelectedCypherText");
                return "error";       
        } 
    }
    
    /**
     * get the crypt text
     */
    
    /**
     * convert the crypt text to hex String
     */
    private static String getHexFromAscii (String input){
        //Used example from: https://www.boraji.com/how-to-convert-ascii-to-hex-in-java
        
        //convert the ascii string to a charcter array
        char[] asciiChar = input.toCharArray();
        
        StringBuilder builder = new StringBuilder();
        
        for(int i =0; i< asciiChar.length; i++){
            //convert interger value to hex 
            int j = (int) asciiChar[i];
            builder.append(Integer.toHexString(j).toUpperCase());
        }
        
        return builder.toString();
        
    } 
    
    /**
     * convert a hex string to ascii
     */
    private static String getAsciiFromHex (String input){
        StringBuilder asciiString = new StringBuilder();
        for(int i=0; i<input.length(); i+=2){
            String temp = input.substring(i,i+2);
            asciiString.append((char) Integer.parseInt(temp, 16));
        }
        
        return asciiString.toString();
    }
    
    /**
     * accepts two strings and if the first string is longer returns an trimmed version of it 
     */
    private static String getTrimmedString (String input1, String input2){
        //get the length of the shortest string 
        if(input1.length() > input2.length()){
            StringBuilder sb = new StringBuilder();
            String trimmedString;
            for(int i = 0; i < input2.length(); i++){
               sb.append(input1.charAt(i));
            }
            trimmedString = sb.toString();
            return trimmedString;
        }
        return input1;
        
    }
    
    /**
     * converts a hex string to an array list of integers 
     */
    private static ArrayList<Integer> getIntValuesFromHexString(String input){
        ArrayList<Integer> intValues = new ArrayList<>();
        for(int i = 0; i < input.length(); i++){
            char tempChar = input.charAt(i);
            intValues.add(getIntFromHex(tempChar));
        }
        return intValues;   
    }
    
    /**
     * convert hex to number 
     */
    private static int getIntFromHex(char charHexNum){
        if (charHexNum >= '0' && charHexNum <= '9')
            return charHexNum - '0';
        if (charHexNum >= 'A' && charHexNum <= 'F')
            return charHexNum - 'A' + 10;
        if (charHexNum >= 'a' && charHexNum <= 'f')
            return charHexNum - 'a' + 10;
        return -1;
    }
    
    /**
     * XORs two arrayLists and returns the XORed results as an arrayList 
     */
    private static ArrayList<Integer> getXorValues(ArrayList<Integer> input1, ArrayList<Integer> input2){
        ArrayList<Integer> results = new ArrayList<>();             
        
        for(int i = 0; i < input1.size(); i++){
            results.add(input1.get(i)^input2.get(i));
            //System.out.println(input1.get(i) + " Xor " + input2.get(i) + " = " + results.get(i) );
        }
        return results;
    }
    
    /**
     * convert integer arrayList back to a hex string 
     */
    private static String getHexStringFromIntArray(ArrayList<Integer> input){
        StringBuilder sb = new StringBuilder();
        String hexString;
        for(int i = 0; i < input.size(); i++){
            sb.append(getHexFromInt(input.get(i)));          
        }
        hexString = sb.toString();
        return hexString;
        
    }
    
    /**
     * convert integer to hex char
     */
    private static char getHexFromInt(int input){
        if(input < 10){
            return Character.forDigit(input, 10);
        }
        if(input == 10){
            char value = 'a';
            return value;
        }
        if(input == 11){
            char value = 'b';
            return value;
        }
        if(input == 12){
            char value = 'c';
            return value;
        }
        if(input == 13){
            char value = 'd';
            return value;
        }
        if(input == 14){
            char value = 'e';
            return value;
        }
        if(input == 15){
            char value = 'f';
            return value;
        }
        System.err.println("error in getHexFromInt");
        return '~';
    }
    
    /**
     * ask user what crypt text to use and returns the inputed string 
     */
    private static String getCribText(){
        Scanner cribTextInput = new Scanner(System.in); 
        System.out.println("Enter the firt cryb text to use (enter exit123 to close the program): ");
        String cribText = cribTextInput.nextLine();
       
       // cribTextInput.close();
        return cribText;
    }
    
    /**
     * cryptDragging function
     */
    private static void StartDragging(String XORedString, String cribText){
       
        int cribTextSize = cribText.length()*2;
        System.out.println("cribtext = " + cribText);
        
        for(int i = 0; i< (XORedString.length()- cribTextSize) ; i+=2){
            String tempTrimedXORedString = XORedString.substring(i,i+cribTextSize);
            
            //converts hex string to integer arrayList then XORs the interger values and converts it back to a hex string
            String dragResult;
            ArrayList<Integer> xORedIntArray = new ArrayList<>();
            ArrayList<Integer> cribIntArray = new ArrayList<>();
            ArrayList<Integer> XorValuesArray = new ArrayList<>();
            
            xORedIntArray = getIntValuesFromHexString(tempTrimedXORedString);
            cribIntArray = getIntValuesFromHexString(getHexFromAscii(cribText));
            XorValuesArray = getXorValues(xORedIntArray , cribIntArray);
            
            /*System.out.print("xored values array is: ");
            for(int j = 0; j<XorValuesArray.size(); j++){
                System.out.print(XorValuesArray.get(j));
                
            }
            System.out.print("\n"); */
            String hexResults = getHexStringFromIntArray(XorValuesArray);
            //System.out.println("hex results = " + hexResults);
            dragResult = getAsciiFromHex(hexResults);
            //System.out.println("ascii results = " + dragResult);
            
            
            if(!checkForNonstandardChars(dragResult)){
                int SecondIndex = i+cribTextSize;
                System.out.println("The results at index[" + i/2 + "] to index [" + SecondIndex/2 + "] is: \"" + dragResult + "\"");
                System.out.print("\n");
                
            }
        } 
        

    }
    
    /**
     * Filter the Results Returns True if string contains a non standard char 
     */
    private static boolean checkForNonstandardChars(String input){
        for(int i = 0; i<input.length(); i++){
            if((int) input.charAt(i) > 126 || (int) input.charAt(i) < 32){
                return true;
            }
        }
        return false;
    }
}
